import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SpinnerComponent from "./components/SpinnerComponent/SpinnerComponent";
import UserManagePage from "./pages/UserManagePage/UserManagePage";
import LoginPage from "./pages/LoginPage/LoginPage";
import ThemeLayout from "./HOC/ThemeLayout";
import LocationManagePage from "./pages/LocationManagePage/LocationManagePage";
import RoomManagePage from "./pages/RoomManagePage/RoomManagePage";
import "./StyleLogin.css";
import ProfileAdminPage from "./pages/ProfileAdminPage/ProfileAdminPage";
import HeaderAdmin from "./HOC/HeaderTheme/HeaderAdmin";

function App() {
  return (
    <div className="App">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/profile" element={<ProfileAdminPage />} />
          <Route
            path="/user"
            element={<ThemeLayout Component={UserManagePage} />}
          />
          <Route
            path="/location"
            element={<ThemeLayout Component={LocationManagePage} />}
          />
          <Route
            path="/room"
            element={<ThemeLayout Component={RoomManagePage} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
