import { CaretDownOutlined } from "@ant-design/icons";
import { Avatar, Popover } from "antd";

import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

import { localStorageService } from "../../services/localStorageService";

export default function HeaderAdmin() {
  let handleLogout = () => {
    localStorageService.removeUserInfor();
    window.location.href = "/";
  };

  let userInfor = useSelector((state) => state.userSlice.userInfor);

  return (
    <div>
      {userInfor ? (
        <div className="flex justify-end items-center h-12 font-medium text-black rounded-md shadow-lg pl-4 pr-5 space-x-4 border-2 border-rose-400 ">
          <div className="">Xin chào {userInfor.name}</div>
          <Avatar src={userInfor.avatar} />
          <Popover
            placement="bottom"
            content={
              <div className="space-y-1 ">
                <NavLink to="/profile">
                  <div className="hover:cursor-pointer hover:text-red-400 border-b-2 border-black pb-3 text-black">
                    Cập nhật thông tin
                  </div>
                </NavLink>
                <div
                  className="hover:cursor-pointer hover:text-red-400 pt-2"
                  onClick={handleLogout}
                >
                  Đăng xuất
                </div>
              </div>
            }
            trigger="click"
          >
            <CaretDownOutlined />
          </Popover>
        </div>
      ) : (
        <div className="space-x-3">
          <NavLink to="/">
            <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
              Đăng nhập
            </button>
          </NavLink>
        </div>
      )}
    </div>
  );
}
