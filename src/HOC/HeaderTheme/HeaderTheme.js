import React from "react";
import { NavLink } from "react-router-dom";
import logo from "../../assets/img/airbnb-logo.png";
import HeaderAdmin from "./HeaderAdmin";

export default function HeaderTheme() {
  return (
    <div className="h-32 w-full flex items-center justify-between shadow-lg px-20 ">
      <div>
        <NavLink to="/user">
          <img className="w-40 h-12" src={logo} alt={logo} />
        </NavLink>
      </div>
      <HeaderAdmin />
    </div>
  );
}
