import React from "react";
import { NavLink } from "react-router-dom";

export default function SidebarTheme() {
  let activeStyle = "text-rose-500 hover:text-rose-500";

  let activeClassName = "no-underline";

  return (
    <div className="fixed left-0 bottom-0 top-32 h-full w-60 ">
      <div className="h-full w-full shadow-md">
        <div className="  pt-44 flex flex-col justify-start space-y-8">
          <nav className="text-lg  font-medium ">
            <NavLink
              to="/user"
              className={({ isActive }) =>
                isActive ? activeStyle : activeClassName
              }
            >
              Quản lí người dùng
            </NavLink>
          </nav>
          <nav className="text-lg font-medium ">
            <NavLink
              to="/location"
              className={({ isActive }) =>
                isActive ? activeStyle : activeClassName
              }
            >
              Quản lí thông tin vị trí
            </NavLink>
          </nav>
          <nav className="text-lg font-medium ">
            <NavLink
              to="/room"
              className={({ isActive }) =>
                isActive ? activeStyle : activeClassName
              }
            >
              Quản lí thông tin phòng
            </NavLink>
          </nav>
        </div>
      </div>
    </div>
  );
}
