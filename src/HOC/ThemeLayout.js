import React from "react";
import HeaderTheme from "./HeaderTheme/HeaderTheme";
import SidebarTheme from "./SidebarTheme/SidebarTheme";

export default function ThemeLayout({ Component }) {
  return (
    <div>
      <HeaderTheme />
      <div className=" w-full h-full">
        <div className="  mt-8 h-full  ">
          <SidebarTheme />
        </div>
        <div className=" shadow-md">
          <Component />
        </div>
      </div>
    </div>
  );
}
