import { Button } from "antd";
import ModalEditLocation from "../../pages/LocationManagePage/FormEditLocation/ModalEditLocation";
export const headerTableLocation = [
  {
    title: "Địa danh",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Tỉnh thành",
    dataIndex: "province",
    key: "province",
  },

  {
    title: "Hình ảnh",
    dataIndex: "image",
    key: "image",
    align: "center",
    render: (img) => {
      return (
        <div className="flex justify-center">
          <img className="h-16 w-24" src={img} alt={img} />
        </div>
      );
    },
  },

  {
    title: "Quốc gia",
    dataIndex: "country",
    key: "country",
  },

  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
    render: ({ onDelete, onUpdate }, record) => {
      return (
        <div className="flex">
          <div>
            <ModalEditLocation record={record} onUpdate={onUpdate} />
          </div>

          <Button
            className=" rounded-md"
            onClick={() => {
              onDelete();
            }}
            type="primary"
            danger
          >
            Xóa
          </Button>
        </div>
      );
    },
  },
];
