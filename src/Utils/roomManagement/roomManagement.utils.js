import { Tag, Button } from "antd";
import ModalEditRoom from "../../pages/RoomManagePage/FormEditRoom/ModalEditRoom";
export const headerTableRoom = [
  {
    title: "Tên phòng",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Địa điểm",
    dataIndex: "locationId",
    key: "locationId",

    align: "center",
    render: (locationId) => {
      return (
        <div className="flex flex-wrap justify-center items-center">
          <p className="w-full">{locationId?.name}</p>
          <p className="w-full">{locationId?.province}</p>
        </div>
      );
    },
  },

  {
    title: "Hình ảnh",
    dataIndex: "image",
    key: "image",

    align: "center",
    render: (img) => {
      return (
        <div className="flex justify-center">
          <img className="h-16 w-24" src={img} alt={img} />
        </div>
      );
    },
  },

  {
    title: "Giá thuê",
    dataIndex: "price",
    key: "price",
  },

  {
    title: "Số lượng khách",
    dataIndex: "guests",
    key: "guests",
    align: "center",
  },

  {
    title: "Thao tác",
    dataIndex: "action",

    key: "action",
    render: ({ onDelete, onUpdate, onUpload }, record) => {
      return (
        <div className="flex">
          <div>
            <ModalEditRoom
              onUpload={onUpload}
              record={record}
              onUpdate={onUpdate}
            />
          </div>

          <Button
            className=" rounded-md"
            onClick={() => {
              onDelete();
            }}
            type="primary"
            danger
          >
            Xóa
          </Button>
        </div>
      );
    },
  },
];
