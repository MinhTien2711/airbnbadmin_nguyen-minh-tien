import { Tag, Button } from "antd";
import ModalEdit from "../../pages/UserManagePage/FormEdit/ModalEdit";
export const headerTableUser = [
  {
    title: "Họ tên",
    dataIndex: "name",
    key: "name",
    align: "center",
    width: 150,
  },
  {
    title: "Giới tính",
    dataIndex: "gender",
    key: "gender",
    align: "center",
    render: (type) => {
      if (type) {
        return <Tag color="cyan">Male</Tag>;
      } else {
        return <Tag color="orange">Female</Tag>;
      }
    },
  },

  {
    title: "Email",
    dataIndex: "email",
    key: "email",
    align: "center",
  },

  {
    title: "Hình đại diện",
    dataIndex: "avatar",
    key: "avatar",
    align: "center",
    render: (img) => {
      return (
        <div className="flex justify-center">
          <img className="h-16 w-24" src={img} alt={img} />
        </div>
      );
    },
  },

  {
    title: "Loại người dùng",
    dataIndex: "type",
    key: "type",
    align: "center",

    render: (type) => {
      if (type == "ADMIN") {
        return <Tag color="cyan">ADMIN</Tag>;
      } else {
        return <Tag color="orange">CLIENT</Tag>;
      }
    },
  },
  {
    title: "Thao tác",
    dataIndex: "action",
    key: "action",
    align: "center",
    render: ({ onDelete, onUpdate }, record) => {
      return (
        <div className="flex">
          <div>
            <ModalEdit record={record} onUpdate={onUpdate} />
          </div>

          <Button
            className=" rounded-md"
            onClick={() => {
              onDelete();
            }}
            type="primary"
            danger
          >
            Xóa
          </Button>
        </div>
      );
    },
  },
];
