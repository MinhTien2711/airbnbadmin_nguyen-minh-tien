import React from "react";
import { RingLoader } from "react-spinners";
import { useSelector } from "react-redux";

export default function SpinnerComponent() {
  const { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div className="fixed h-screen w-screen bg-black z-50 flex items-center justify-center">
      <RingLoader size={150} color={"#FFA500"} />
    </div>
  ) : (
    <></>
  );
}
