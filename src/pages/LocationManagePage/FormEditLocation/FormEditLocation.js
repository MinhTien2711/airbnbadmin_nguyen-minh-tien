import React from "react";
import { Form, Input, message, Image } from "antd";
import { useDispatch } from "react-redux";

export default function FormEditLocation({ record, onUpdate, closeModal }) {
  const validateStatus = {
    required: "${label} không được để trống",
  };
  //   let dispatch = useDispatch();

  const onFinish = (values) => {
    onUpdate(values);
    // dispatch(uploadImageActionService(values)).then((resDispatch) => {
    //   console.log("resDispatch: ", resDispatch);
    // });
    setTimeout(() => {
      closeModal();
    }, 3000);
  };

  const onFinishFailed = () => {
    message.error("Sửa thông tin không thành công");
  };

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 7 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            initialValue={record.name}
            label="Địa danh"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={record.province}
            label="Tỉnh thành"
            name="province"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={record.country}
            label="Quốc gia"
            name="country"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item initialValue={record.image} label="Hình ảnh" name="image">
            <Image width={300} src={record.image} />
          </Form.Item>

          <Form.Item>
            <button className="py-3 px-7   text-white w-full  rounded-lg font-medium text-lg button-hover ">
              <span>Cập nhật</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
