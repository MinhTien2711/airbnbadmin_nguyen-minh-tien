import React from "react";
import { Form, Input } from "antd";
import { useDispatch } from "react-redux";
import { addLocationActionService } from "../../../redux/slices/locationSlice";

export default function FormLocation({ closeModal, fetchLocationList }) {
  const dispatch = useDispatch();

  const validateStatus = {
    required: "${label} không được để trống",
  };

  const onFinish = (values) => {
    dispatch(addLocationActionService(values));
    setTimeout(() => {
      closeModal();
      fetchLocationList();
    }, 2000);
  };

  const onFinishFailed = () => {};

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 18 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Địa danh"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Tỉnh thành"
            name="province"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Quốc gia"
            name="country"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item>
            <button className="py-3 px-7 ml-20 mt-4 text-white w-full  rounded-lg font-medium text-lg button-hover  ">
              <span>Thêm địa điểm</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
