import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux/slices/spinnerSlice";
import { locationService } from "../../services/locationService";
import ModalAddLocation from "./FormLocation/ModalAddLocation";

import TableLocation from "./TableLocation/TableLocation";

export default function UserManagePage() {
  const [locationList, setLocationList] = useState([]);
  const [valueSearch, setValueSearch] = useState();
  let dispatch = useDispatch();

  let fetchLocationList = () => {
    locationService
      .getLocationList()
      .then((res) => {
        let dataRaw = res.data.map((user) => {
          return {
            ...user,
            action: {
              onDelete: () => {
                dispatch(setSpinnerStarted());
                locationService
                  .deleteLocation(user._id)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Xóa thành công");
                      fetchLocationList();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },

              onUpdate: (data) => {
                dispatch(setSpinnerStarted());
                locationService
                  .updateLocation(user._id, data)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Cập nhật thành công");
                      fetchLocationList();
                    }, 1000);
                  })
                  .catch((err) => {});
              },
            },
          };
        });
        setLocationList(dataRaw);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchLocationList();
  }, []);
  useEffect(() => {
    let handleChange = setTimeout(() => {
      if (valueSearch) {
        let value = locationList.filter((item) => {
          return (
            item.name && item.name.trim().toUpperCase().includes(valueSearch)
          );
        });

        setLocationList(value);
      } else {
        return fetchLocationList();
      }
    }, 1000);
    return () => clearTimeout(handleChange);
  }, [valueSearch]);

  return (
    <div className=" pl-72 pr-12  space-y-5">
      <div>
        <ModalAddLocation fetchLocationList={fetchLocationList} />
      </div>
      <div className="flex justify-center space-x-4 mt-5">
        <input
          className="w-full h-12 border-2 border-black rounded-md px-5 text-lg"
          type="text"
          placeholder="Nhập vào địa danh bạn muốn tìm"
          name="search"
          onChange={(event) =>
            setValueSearch(event.target.value.trim().toUpperCase())
          }
        />
      </div>
      <div className="container mx-auto">
        <TableLocation locationList={locationList} />
      </div>
    </div>
  );
}
