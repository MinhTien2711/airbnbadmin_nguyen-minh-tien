import { Table } from "antd";
import { headerTableLocation } from "../../../Utils/locationManagement/locationManagement.utils";

const TableLocation = ({ locationList }) => (
  <Table bordered columns={headerTableLocation} dataSource={locationList} />
);

export default TableLocation;
