import { Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  setUserInfor,
  setUserLoginActionService,
  setUserToken,
} from "../../redux/slices/userSlice";
import { localStorageService } from "../../services/localStorageService";

import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux/slices/spinnerSlice";
const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const validateStatus = {
    required: "${label} không được để trống",
    types: {
      email: "${label} không hợp lệ",
    },
  };
  const onFinish = (values) => {
    // unwrap ~ handle dispatch thành promise
    dispatch(setUserLoginActionService(values))
      .unwrap()
      .then((resDispatch) => {
        dispatch(setSpinnerStarted());
        if (resDispatch && resDispatch.type == "ADMIN") {
          localStorageService.setUserInfor(resDispatch);
          dispatch(setUserInfor(resDispatch));
          setTimeout(() => {
            message.success("Đăng nhập thành công");
            dispatch(setSpinnerEnded());

            navigate("/user");
          }, 1000);
        } else if (resDispatch && resDispatch.type == "CLIENT") {
          setTimeout(() => {
            dispatch(setSpinnerEnded());
            message.error("Bạn không phải là admin");
          }, 1000);
        } else {
          setTimeout(() => {
            dispatch(setSpinnerEnded());
          }, 1000);
        }
      });
  };

  const onFinishFailed = (errorInfo) => {};

  return (
    <div className="   relative">
      <div className=" bg-login absolute w-screen h-screen  ">
        <div className="relative container mx-auto my-44 z-10 p-2 rounded-xl justify-center items-center flex flex-col bg-form">
          <div className=" h-16 flex  border-b font-bold text-xl uppercase text-red-500">
            Đăng nhập
          </div>
          <div className="p-4">
            <div className="mb-4 font-bold text-2xl text-red-500">
              Chào mừng bạn đến với Airbnb
            </div>

            <Form
              name="basic"
              className="font-medium"
              labelCol={{ span: 5 }}
              style={{ width: "450px" }}
              initialValues={{
                remember: true,
              }}
              validateMessages={validateStatus}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Email"
                name="email"
                hasFeedback
                rules={[
                  {
                    required: true,
                    type: "email",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Xin vui lòng nhập mật khẩu!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item>
                <button className="p-3 ml-10 mt-4  text-white w-1/2 rounded-lg font-medium text-lg button-hover">
                  Đăng nhập
                </button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
