import React from "react";

import logo from "../../assets/img/airbnb-logo.png";

import { CaretDownOutlined } from "@ant-design/icons";
import { Avatar, Popover } from "antd";

import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

import { localStorageService } from "../../services/localStorageService";

export default function HeaderProfile({ userReload }) {
  let handleLogout = () => {
    localStorageService.removeUserInfor();
    window.location.href = "/";
  };

  let userInfor = useSelector((state) => state.userSlice.userInfor);
  return (
    <div className="h-32 w-full flex items-center justify-between shadow-lg px-20 ">
      <div>
        <NavLink to="/user">
          <img className="w-40 h-12" src={logo} alt={logo} />
        </NavLink>
      </div>
      <div>
        {userReload ? (
          <div className="flex">
            <div className="space-x-3">
              <NavLink to="/user">
                <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
                  Quay lại trang chủ
                </button>
              </NavLink>
            </div>
            <div className="flex justify-end items-center h-12 font-medium text-black   pl-4 pr-5 space-x-4  ">
              <Avatar src={userReload.avatar} />
              <Popover
                placement="bottom"
                content={
                  <div className="space-y-1 ">
                    <div
                      className="hover:cursor-pointer hover:text-red-400 pt-2"
                      onClick={handleLogout}
                    >
                      Đăng xuất
                    </div>
                  </div>
                }
                trigger="click"
              >
                <CaretDownOutlined />
              </Popover>
            </div>
          </div>
        ) : (
          <div className="flex">
            <div className="space-x-3">
              <NavLink to="/user">
                <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
                  Quay lại trang chủ
                </button>
              </NavLink>
            </div>
            <div className="flex justify-end items-center h-12 font-medium text-black   pl-4 pr-5 space-x-4  ">
              <Avatar src={userInfor.avatar} />
              <Popover
                placement="bottom"
                content={
                  <div className="space-y-1 ">
                    <div
                      className="hover:cursor-pointer hover:text-red-400 pt-2"
                      onClick={handleLogout}
                    >
                      Đăng xuất
                    </div>
                  </div>
                }
                trigger="click"
              >
                <CaretDownOutlined />
              </Popover>
            </div>
          </div>
          // <div className="space-x-3">
          //   <NavLink to="/">
          //     <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
          //       Đăng nhập
          //     </button>
          //   </NavLink>
          // </div>
        )}
      </div>
      {/* {userReload && userReload ? (
        <div className="flex">
          <div className="space-x-3">
            <NavLink to="/user">
              <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
                Quay lại trang chủ
              </button>
            </NavLink>
          </div>
          <div className="flex justify-end items-center h-12 font-medium text-black   pl-4 pr-5 space-x-4  ">
            <Avatar src={userReload.avatar} />
            <Popover
              placement="bottom"
              content={
                <div className="space-y-1 ">
                  <div
                    className="hover:cursor-pointer hover:text-red-400 pt-2"
                    onClick={handleLogout}
                  >
                    Đăng xuất
                  </div>
                </div>
              }
              trigger="click"
            >
              <CaretDownOutlined />
            </Popover>
          </div>
        </div>
      ) : (
        <div className="space-x-3">
          <NavLink to="/">
            <button className=" border-blue-700 border-2 px-4 py-2 rounded text-blue-700">
              Đăng nhập
            </button>
          </NavLink>
        </div>
      )} */}
    </div>
  );
}
