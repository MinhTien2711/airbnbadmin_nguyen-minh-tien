import React, { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { format } from "date-fns";
import { message, Upload } from "antd";

import { BASE_URL, TOKEN_CYBERSOFT } from "../../services/configURL";
import { userService } from "../../services/userService";

import HeaderProfile from "./HeaderProfile";
import { localStorageService } from "../../services/localStorageService";
import { setUserAvatar } from "../../redux/slices/userSlice";

export default function ProfileAdminPage() {
  let userInfor = useSelector((state) => state.userSlice.userInfor);

  const [userReload, setUserReload] = useState();

  let dispatch = useDispatch();
  const props = {
    name: "avatar",
    action: `${BASE_URL}/api/users/upload-avatar`,
    headers: {
      token: localStorageService.getUserToken(),
      tokenByClass: TOKEN_CYBERSOFT,
    },

    onChange(info) {
      if (info.file.status !== "uploading") {
      }

      if (info.file.status === "done") {
        message.success("Bạn đã cập nhật hình đại diện thành công");
        setTimeout(() => {
          userService
            .getInforUser(userInfor._id)
            .then((res) => {
              setUserReload(res.data);
              localStorageService.setUserInfor(res.data);
              dispatch(setUserAvatar(res.data));
            })
            .catch((err) => {
              console.log("err", err);
            });
        }, 1000);
        //
      } else if (info.file.status === "error") {
        message.error("Bạn đã cập nhật hình đại diện thất bại");
      }
    },
  };

  return (
    <>
      <HeaderProfile userReload={userReload} />
      <div className=" container mx-auto flex flex-col md:flex-row pt-14 px-5">
        <div className=" w-80 h-fit ml-48 text-left border border-gray-500 rounded-xl shadow-2xl p-8 ">
          <div className="flex items-center justify-center">
            <div className="w-44 h-44 bg-white rounded-full overflow-hidden border">
              {!userReload ? (
                <img
                  src={userInfor.avatar}
                  alt=""
                  className="object-cover object-center w-full h-full"
                />
              ) : (
                <img
                  src={userReload.avatar}
                  alt=""
                  className="object-cover object-center w-full h-full"
                />
              )}
            </div>
          </div>
          <div className="text-center text-sm font-semibold w-full">
            <Upload {...props}>
              <button className="w-max underline mt-2 cursor-pointer ml-3">
                Cập nhật ảnh
              </button>
            </Upload>
          </div>
          <div>
            <h2 className="font-bold mt-5 text-xl">Xác minh danh tính</h2>
            <p className=" text-sm my-3">
              Xác thực danh tính của bạn với huy hiệu xác minh danh tính
            </p>
            <div className="hover:cursor-pointer mt-6 p-4 text-center bg-white font-semibold text-sm rounded-md border border-gray-800">
              Nhận huy hiệu
            </div>

            <div className="py-6 border-t">
              <h3 className=" text-lg mb-5 font-bold">
                <span>Đã xác nhận</span>
              </h3>
              <div className="text-sm">
                <span className="mr-2"></span>
                <span>Địa chỉ email</span>
              </div>
            </div>
          </div>
        </div>
        <div className=" w-full md:w-3/5 px-5 md:pl-4 ml-40">
          <div className=" flex flex-row justify-between items-center pb-6  ">
            <div className="text-left">
              <h2 className="text-2xl font-semibold mb-2">
                Xin chào, tôi là {userInfor ? userInfor.name : "Admin"}
              </h2>
              <p className="text-sm font-light text-gray-700 my-4 ">
                <span> Bắt đầu tham gia vào 2021 </span>
              </p>
              <div className="py-5 overflow-x-auto relative">
                {userInfor ? (
                  <table className="table-auto w-full text-sm text-left text-gray-500 ">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 border-b">
                      <tr>
                        <th scope="col" className="py-3 px-9">
                          Mục
                        </th>
                        <th scope="col" className="py-3 px-9">
                          Thông tin
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Tên
                        </th>
                        <td className="py-4 px-9">{userInfor.name}</td>
                      </tr>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Email
                        </th>
                        <td className="py-4 px-9">{userInfor.email}</td>
                      </tr>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Giới tính
                        </th>
                        <td className="py-4 px-9">
                          {userInfor.gender ? "Nam" : "Nữ"}
                        </td>
                      </tr>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Điện thoại
                        </th>
                        <td className="py-4 px-9">{userInfor.phone}</td>
                      </tr>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Ngày sinh
                        </th>
                        <td className="py-4 px-9">
                          {format(new Date(userInfor.birthday), "dd/MM/yyyy")}
                        </td>
                      </tr>
                      <tr className="bg-white border-b  ">
                        <th
                          scope="row"
                          className="py-4 px-9 font-medium text-gray-900 whitespace-nowrap "
                        >
                          Địa chỉ
                        </th>
                        <td className="py-4 px-9">{userInfor.address}</td>
                      </tr>
                    </tbody>
                  </table>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div className=" py-8 border-b ">
            <div className="text-sm text-left font-semibold w-full">
              <button className="w-max underline">Đánh giá của bạn</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
