import React, { useState } from "react";
import { Form, Input, DatePicker, Select, message, Image } from "antd";
import moment from "moment";
import { useDispatch } from "react-redux";
import { uploadImageActionService } from "../../../redux/slices/userSlice";
import UploadImage from "./UploadImage";

const { Option } = Select;
const { TextArea } = Input;

export default function FormEditRoom({
  record,
  onUpdate,
  closeModal,
  onUpload,
}) {
  const validateStatus = {
    required: "${label} không được để trống",
  };

  const onFinish = (values) => {
    // onUpload(values);
    onUpdate(values);

    setTimeout(() => {
      closeModal();
    }, 2000);
    console.log("values: ", values);
  };

  const onFinishFailed = () => {
    message.error("Sửa thông tin không thành công");
  };

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 7 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            initialValue={record.name}
            label="Tên phòng"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          {record.price ? (
            <Form.Item
              initialValue={record.price}
              label="Giá thuê"
              name="price"
              hasFeedback
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
          ) : null}
          {record.bath ? (
            <Form.Item
              initialValue={record.bath}
              label="Phòng tắm"
              name="bath"
              hasFeedback
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
          ) : null}
          {record.bedRoom ? (
            <Form.Item
              initialValue={record.bedRoom}
              label="Phòng ngủ"
              name="bedRoom"
              hasFeedback
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
          ) : null}
          <Form.Item
            initialValue={record.cableTV ? record.cableTV : false}
            label="Truyền hình cáp"
            name="cableTV"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.dryer ? record.dryer : false}
            label="Máy sấy tóc"
            name="dryer"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.elevator ? record.elevator : false}
            label="Thang máy"
            name="elevator"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.gym ? record.gym : false}
            label="Phòng gym"
            name="gym"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.heating ? record.heating : false}
            label="Lò sưởi"
            name="heating"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.pool ? record.pool : false}
            label="Hồ bơi"
            name="pool"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.wifi ? record.wifi : false}
            label="Wifi"
            name="wifi"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.kitchen ? record.kitchen : false}
            label="Nhà bếp"
            name="kitchen"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>

          <Form.Item
            initialValue={record.description}
            label="Mô tả"
            name="description"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <TextArea rows={5} />
          </Form.Item>
          <Form.Item label="Hình ảnh phòng">
            {/* <UploadImage image={record.image} id={record._id} /> */}
            <Image
              width={300}
              src={record.image} //Display new uploaded image replace old one
            />
          </Form.Item>

          <Form.Item>
            <button className="py-3 px-7   text-white w-full  rounded-lg font-medium text-lg button-hover ">
              <span>Cập nhật</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
