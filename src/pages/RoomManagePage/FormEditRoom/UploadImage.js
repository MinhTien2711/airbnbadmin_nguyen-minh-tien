// import React, { useEffect, useState } from "react";

// import { useDispatch, useSelector } from "react-redux";

// import { message, Upload } from "antd";

// import { roomService } from "../../../services/roomService";
// import { localStorageService } from "../../../services/localStorageService";
// import { BASE_URL, TOKEN_CYBERSOFT } from "../../../services/configURL";

// export default function UploadImage({ id, image }) {
//   const [roomReload, setRoomReload] = useState();

//   let dispatch = useDispatch();
//   const props = {
//     name: "image",
//     action: `${BASE_URL}/api/rooms/upload-image/${id}`,
//     headers: {
//       token: localStorageService.getUserToken(),
//       tokenByClass: TOKEN_CYBERSOFT,
//     },

//     onChange(info) {
//       if (info.file.status !== "uploading") {
//       }

//       if (info.file.status === "done") {
//         message.success("Bạn đã cập nhật hình đại diện thành công");
//         // // setTimeout(() => {
//         // //   roomService
//         // //     .getInforRoom(id)
//         // //     .then((res) => {
//         // //       console.log("res: ", res);
//         // //       setRoomReload(res.data);

//         //       // dispatch(setUserAvatar(res.data));
//         //     })
//         //     .catch((err) => {
//         //       console.log("err", err);
//         //     });
//         // }, 1000);
//         //
//       } else if (info.file.status === "error") {
//         message.error("Bạn đã cập nhật hình đại diện thất bại");
//       }
//     },
//   };

//   return (
//     <>
//       <div className=" container mx-auto flex flex-col  pt-14 px-5">
//         {!roomReload ? (
//           <img
//             src={image}
//             alt=""
//             className="object-cover object-center w-full h-full"
//           />
//         ) : (
//           <img
//             src={roomReload.image}
//             alt=""
//             className="object-cover object-center w-full h-full"
//           />
//         )}
//         <div className="text-center text-sm font-semibold w-full">
//           <Upload {...props}>
//             <button className="w-max underline mt-2 cursor-pointer ml-3">
//               Cập nhật ảnh
//             </button>
//           </Upload>
//         </div>
//       </div>
//     </>
//   );
// }
import { UploadOutlined } from "@ant-design/icons";
import { Button, message, Upload } from "antd";
import React from "react";
const props = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },

  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }

    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

const UploadImage = () => (
  <Upload {...props}>
    <Button icon={<UploadOutlined />}>Click to Upload</Button>
  </Upload>
);

export default UploadImage;
