import React, { useState } from "react";
import { Form, Input, DatePicker, Select, message } from "antd";
import moment from "moment";
import { useDispatch } from "react-redux";
import { uploadImageActionService } from "../../../redux/slices/userSlice";
import { addRoomActionService } from "../../../redux/slices/roomSlice";
// import UploadImage from "./UploadImage";

const { Option } = Select;
const { TextArea } = Input;

export default function FormUserRoom({
  closeModal,
  fetchRoomList,
  // onUpload,
}) {
  const validateStatus = {
    required: "${label} không được để trống",
  };
  const dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("values: ", values);
    dispatch(addRoomActionService(values));
    setTimeout(() => {
      closeModal();
      fetchRoomList();
    }, 2000);
  };

  const onFinishFailed = () => {
    message.error("Sửa thông tin không thành công");
  };

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 7 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tên phòng"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Giá thuê"
            name="price"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Xin vui lòng nhập kí tự số",
                pattern: /^[0-9]*$/,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Phòng tắm"
            name="bath"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Xin vui lòng nhập kí tự số",
                pattern: /^[0-9]*$/,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Phòng ngủ"
            name="bedRoom"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Xin vui lòng nhập kí tự số",
                pattern: /^[0-9]*$/,
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Truyền hình cáp"
            name="cableTV"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Máy sấy tóc"
            name="dryer"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Thang máy"
            name="elevator"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Phòng gym"
            name="gym"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Lò sưởi"
            name="heating"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Hồ bơi"
            name="pool"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Wifi"
            name="wifi"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="Nhà bếp"
            name="kitchen"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value={true}>Có</Option>
              <Option value={false}>Không có</Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Mô tả"
            name="description"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <TextArea rows={5} />
          </Form.Item>
          {/* <Form.Item label="Hình ảnh phòng">
            <img
              className="w-96"
              src={record.image} //Display new uploaded image replace old one
            />
            <input
              type="file"
              id="upload-photo"
              className="absolute top-0 left-0 -z-10 cursor-none"
            />
          </Form.Item> */}
          <div className="w-full flex justify-between mt-5 mb-2">
            <label
              htmlFor="upload-photo"
              className="border border-gray-300 px-3 py-1 my-auto cursor-pointer hover:bg-gray-200"
            >
              Tải hình ảnh mới
            </label>
          </div>
          <Form.Item>
            <button className="py-3 px-7   text-white w-full  rounded-lg font-medium text-lg button-hover ">
              <span>Cập nhật</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
