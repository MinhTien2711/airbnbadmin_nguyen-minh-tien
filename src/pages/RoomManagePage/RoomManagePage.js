import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux/slices/spinnerSlice";
import { roomService } from "../../services/roomService";
import ModalAddRoom from "./FormRoom/ModalAddRoom";

import TableRoom from "./TableRoom/TableRoom";

export default function RoomManagePage() {
  const [roomList, setRoomList] = useState([]);
  const [valueSearch, setValueSearch] = useState();

  let dispatch = useDispatch();
  let fetchRoomList = () => {
    roomService
      .getRoomList()
      .then((res) => {
        let dataRaw = res.data.map((user) => {
          return {
            ...user,
            action: {
              onDelete: () => {
                dispatch(setSpinnerStarted());
                roomService
                  .deleteRoom(user._id)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Xóa thành công");
                      fetchRoomList();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },
              onUpdate: (data) => {
                dispatch(setSpinnerStarted());
                roomService
                  .updateRoom(user._id, data)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Cập nhật thành công");
                      fetchRoomList();
                    }, 1000);
                  })
                  .catch((err) => {});
              },
              onUpload: () => {
                roomService
                  .uploadRoomImage(user._id)
                  .then((res) => {
                    message.success("Cập nhật thành công");
                    fetchRoomList();
                  })
                  .catch((err) => {
                    console.log("err: ", err);
                  });
              },
            },
          };
        });
        setRoomList(dataRaw);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetchRoomList();
  }, []);

  useEffect(() => {
    let handleChange = setTimeout(() => {
      if (valueSearch) {
        let value = roomList.filter((item) => {
          return (
            item.name && item.name.trim().toUpperCase().includes(valueSearch)
          );
        });

        setRoomList(value);
      } else {
        return fetchRoomList();
      }
    }, 1000);
    return () => clearTimeout(handleChange);
  }, [valueSearch]);

  return (
    <div className=" pl-72 pr-12  space-y-5">
      <div>
        <ModalAddRoom fetchRoomList={fetchRoomList} />
      </div>
      <div className="flex justify-center space-x-4 mt-5">
        <input
          className="w-full h-12 border-2 border-black rounded-md px-5 text-lg"
          type="text"
          placeholder="Nhập vào tên phòng bạn muốn tìm"
          name="search"
          onChange={(event) =>
            setValueSearch(event.target.value.trim().toUpperCase())
          }
        />
      </div>
      <div className="container mx-auto">
        <TableRoom roomList={roomList} />
      </div>
    </div>
  );
}
