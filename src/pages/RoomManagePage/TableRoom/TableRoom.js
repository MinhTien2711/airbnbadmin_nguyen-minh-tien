import { Table } from "antd";
import { headerTableRoom } from "../../../Utils/roomManagement/roomManagement.utils";

const TableRoom = ({ roomList }) => (
  <Table bordered columns={headerTableRoom} dataSource={roomList} />
);

export default TableRoom;
