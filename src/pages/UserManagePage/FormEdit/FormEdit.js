import React from "react";
import { Form, Input, DatePicker, Select, message, Upload } from "antd";
import moment from "moment";
import { useDispatch } from "react-redux";
import { uploadImageActionService } from "../../../redux/slices/userSlice";

const { Option } = Select;

export default function FormEdit({ record, onUpdate, closeModal }) {
  const validateStatus = {
    required: "${label} không được để trống",
    types: {
      email: "${label} không hợp lệ",
      number: "${label} không hợp lệ",
    },
  };
  let dispatch = useDispatch();

  const onFinish = (values) => {
    onUpdate(values);
    dispatch(uploadImageActionService(values)).then((resDispatch) => {
      console.log("resDispatch: ", resDispatch);
    });
    setTimeout(() => {
      closeModal();
    }, 2000);
    console.log("values: ", values);
  };

  const onFinishFailed = () => {
    message.error("Sửa thông tin không thành công");
  };

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 7 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            initialValue={record.name}
            label="Họ tên"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={record.email}
            label="Email"
            name="email"
            hasFeedback
            rules={[
              {
                required: true,
                type: "email",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            initialValue={record.password}
            label="Mật khẩu"
            name="password"
            hasFeedback
            rules={[
              {
                required: true,
                min: 6,
                message: "Mật khẩu không được ít hơn 6 chữ số hoặc ký tự",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            initialValue={record.phone}
            label="Số điện thoại"
            name="phone"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Số điện thoại không hợp lệ",
                pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={moment(record?.birthday)}
            label="Ngày sinh"
            name="birthday"
            hasFeedback
            rules={[{ required: true }]}
          >
            <DatePicker
              format="YYYY/MM/DD"
              placeholder="Lựa chọn ngày tháng năm sinh"
              style={{
                width: "100%",
              }}
            />
          </Form.Item>
          <Form.Item
            initialValue={record.gender}
            name="gender"
            label="Giới tính"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select value={record.gender} placeholder="Lựa chọn giới tính">
              <Option value={true}>Male</Option>
              <Option value={false}>Female</Option>
            </Select>
          </Form.Item>
          <Form.Item
            initialValue={record.address}
            name="address"
            label="Địa chỉ"
            hasFeedback
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          {record.avatar ? (
            <Form.Item
              name="avatar"
              initialValue={record.avatar}
              label="Hình đại diện"
            >
              <img className=" h-20" src={record.avatar} alt="" />
            </Form.Item>
          ) : null}
          <Form.Item
            initialValue={record.type}
            name="type"
            label="Mã loại"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value="ADMIN">ADMIN</Option>
              <Option value="CLIENT">CLIENT</Option>
            </Select>
          </Form.Item>

          <Form.Item>
            <button className="py-3 px-7   text-white w-full  rounded-lg font-medium text-lg button-hover ">
              <span>Cập nhật</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
