import React from "react";
import { Form, Input, DatePicker, Select } from "antd";
import { useDispatch } from "react-redux";
import { addUserActionService } from "../../../redux/slices/userSlice";

const { Option } = Select;

export default function FormUsers({ closeModal, fetchUserList }) {
  const dispatch = useDispatch();

  const validateStatus = {
    required: "${label} không được để trống",
    types: {
      email: "${label} không hợp lệ",
      number: "${label} không hợp lệ",
    },
  };

  const onFinish = (values) => {
    dispatch(addUserActionService(values));
    setTimeout(() => {
      closeModal();
      fetchUserList();
    }, 2000);
  };

  const onFinishFailed = () => {};

  return (
    <div className="rounded-xl">
      <div>
        <Form
          className="font-medium"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          validateMessages={validateStatus}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Họ tên"
            name="name"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            hasFeedback
            rules={[
              {
                required: true,
                type: "email",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="password"
            hasFeedback
            rules={[
              {
                required: true,
                min: 6,
                message: "Mật khẩu không được ít hơn 6 chữ số hoặc ký tự",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="confirm"
            label="Nhập lại mật khẩu"
            hasFeedback
            dependencies={["password"]}
            rules={[
              {
                required: true,
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error("Mật khẩu không trùng khớp"));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="Số điện thoại"
            name="phone"
            hasFeedback
            rules={[
              {
                required: true,
              },
              {
                message: "Số điện thoại không hợp lệ",
                pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Ngày sinh"
            name="birthday"
            hasFeedback
            rules={[{ required: true }]}
          >
            <DatePicker
              format="YYYY-MM-DD"
              placeholder="Lựa chọn ngày tháng năm sinh"
              style={{
                width: "100%",
              }}
            />
          </Form.Item>
          <Form.Item
            name="gender"
            label="Giới tính"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select placeholder="Lựa chọn giới tính">
              <Option value={true}>Male</Option>
              <Option value={false}>Female</Option>
            </Select>
          </Form.Item>
          <Form.Item
            name="address"
            hasFeedback
            label="Địa chỉ"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            initialValue={"ADMIN"}
            name="type"
            label="Mã loại"
            hasFeedback
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select>
              <Option value="ADMIN">ADMIN</Option>
            </Select>
          </Form.Item>
          <Form.Item>
            <button className="py-3 px-7 ml-20 mt-4 text-white w-full  rounded-lg font-medium text-lg button-hover  ">
              <span>Thêm quản trị viên</span>
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
