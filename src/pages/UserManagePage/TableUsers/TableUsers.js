import { Table } from "antd";
import { headerTableUser } from "../../../Utils/userManagement/userManagement.utils";

const TableUsers = ({ userList }) => (
  <Table bordered columns={headerTableUser} dataSource={userList} />
);

export default TableUsers;
