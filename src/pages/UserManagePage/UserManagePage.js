import { message } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux/slices/spinnerSlice";

import { userService } from "../../services/userService";
import ModalAdd from "./FormUser/ModalAdd";

import TableUsers from "./TableUsers/TableUsers";

export default function UserManagePage() {
  const [userList, setUserList] = useState([]);
  const [valueSearch, setValueSearch] = useState();

  let dispatch = useDispatch();

  let fetchUserList = () => {
    userService
      .getUserList()
      .then((res) => {
        let dataRaw = res.data.map((user) => {
          return {
            ...user,
            action: {
              onDelete: () => {
                dispatch(setSpinnerStarted());
                userService
                  .deleteUser(user._id)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Xóa thành công");
                      fetchUserList();
                    }, 1000);
                  })
                  .catch((err) => {
                    message.error(err.response.data.content);
                  });
              },

              onUpdate: (data) => {
                dispatch(setSpinnerStarted());
                userService
                  .updateInforUser(user._id, data)
                  .then((res) => {
                    setTimeout(() => {
                      dispatch(setSpinnerEnded());
                      message.success("Cập nhật thành công");
                      fetchUserList();
                    }, 1000);
                  })
                  .catch((err) => {});
              },
            },
          };
        });
        setUserList(dataRaw);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    let handleChange = setTimeout(() => {
      if (valueSearch) {
        let value = userList.filter((item) => {
          return item.email && item.email.includes(valueSearch);
        });
        setUserList(value);
      } else {
        return fetchUserList();
      }
    }, 1000);

    return () => clearTimeout(handleChange);
  }, [valueSearch]);
  useEffect(() => {
    fetchUserList();
  }, []);

  return (
    <div className=" pl-72 pr-12  space-y-5">
      <div>
        <ModalAdd fetchUserList={fetchUserList} />
      </div>
      <div className="flex justify-center space-x-4 mt-5">
        <input
          className="w-full h-12 border-2 border-black rounded-md px-5 text-lg"
          type="text"
          placeholder="Nhập vào email bạn muốn tìm"
          name="search"
          onChange={(event) => setValueSearch(event.target.value)}
        />
      </div>
      <div className="container mx-auto">
        <TableUsers userList={userList} />
      </div>
    </div>
  );
}
