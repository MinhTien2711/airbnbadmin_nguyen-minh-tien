import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import { locationService } from "../../services/locationService";

let initialState = {
  // user: null,

  inforUser: null,
  isOpen: false,
  image: "",
};

export const addLocationActionService = createAsyncThunk(
  "/locationSlice/add",
  async (data) => {
    try {
      let result = await locationService.addLocation(data);
      console.log("result: ", result);
      message.success("Thêm địa điểm thành công");
      return result.data;
    } catch (err) {
      message.error("Thêm địa diểm thất bại");
    }
  }
);

const locationSlice = createSlice({
  name: "locationSlice",
  initialState,

  reducers: {},

  extraReducers: {},
});
// export bỏ vào rootReducer
// export const { openCloseModalEdit } = userSlice.actions;
export default locationSlice.reducer;
