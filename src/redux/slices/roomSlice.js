import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import { roomService } from "../../services/roomService";

let initialState = {
  inforUser: null,

  image: "",
};

export const addRoomActionService = createAsyncThunk(
  "/roomSlice/add",
  async (data) => {
    try {
      let result = await roomService.addRoom(data);
      console.log("result: ", result);
      message.success("Thêm phòng thành công");
      return result.data;
    } catch (err) {
      message.error("Thêm phòng thất bại");
    }
  }
);
export const getRoomActionService = createAsyncThunk(
  "/roomSlice/get",
  async (id) => {
    try {
      let result = await roomService.getInforRoom(id);
      console.log("result: ", result);
      message.success("Thêm phòng thành công");
      return result.data;
    } catch (err) {
      message.error("Thêm phòng thất bại");
    }
  }
);

const roomSlice = createSlice({
  name: "roomSlice",
  initialState,

  reducers: {},

  extraReducers: {},
});
// export bỏ vào rootReducer
// export const { openCloseModalEdit } = userSlice.actions;
export default roomSlice.reducer;
