import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { message } from "antd";
import { localStorageService } from "../../services/localStorageService";

import { userService } from "../../services/userService";

let initialState = {
  userInfor: localStorageService.getUserInfor(),
  userAvatar: localStorageService.getUserInfor(),
  userToken: localStorageService.getUserToken(),
};

export const setUserLoginActionService = createAsyncThunk(
  "/userSlice/login",
  async (dataLogin, thunkAPI) => {
    try {
      let result = await userService.postDangNhap(dataLogin);
      localStorageService.setUserToken(result.data.token);
      thunkAPI.dispatch(setUserToken(result.data.token));

      return result.data.user;
    } catch (err) {
      message.error(err.response.data.message);
    }
  }
);
export const addUserActionService = createAsyncThunk(
  "/userSlice/add",
  async (data) => {
    try {
      let result = await userService.addUser(data);

      message.success("Thêm quản trị viên thành công");
      return result.data;
    } catch (err) {
      message.error("Thêm quản trị viên thất bại");
    }
  }
);
export const uploadImageActionService = createAsyncThunk(
  "/userSlice/upload",
  async (data) => {
    try {
      let result = await userService.uploadImage(data);

      message.success("Upload hình thành công");
      return result.data;
    } catch (err) {
      message.error("Upload thất bại");
    }
  }
);
export const getUserList = createAsyncThunk(
  "userSlice/getUser",
  async (_, thunkAPI) => {
    try {
      const result = await userService.getUserList();

      return result.data;
    } catch {
      return thunkAPI.rejectWithValue();
    }
  }
);

const userSlice = createSlice({
  name: "userSlice",
  initialState,

  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = action.payload;
      return state;
    },
    setUserAvatar: (state, action) => {
      state.userAvatar = action.payload;
      return state;
    },
    setUserToken: (state, action) => {
      state.userToken = action.payload;
    },
  },

  extraReducers: {},
});
// export bỏ vào rootReducer
export const { setUserInfor, setUserToken, setUserAvatar } = userSlice.actions;
export default userSlice.reducer;
