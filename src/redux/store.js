import { configureStore } from "@reduxjs/toolkit";

import userSlice from "./slices/userSlice";
import locationSlice from "./slices/locationSlice";
import spinnerSlice from "./slices/spinnerSlice";
import roomSlice from "./slices/roomSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    spinnerSlice,
    locationSlice,
    roomSlice,
  },
  //   devTools: false,
});
