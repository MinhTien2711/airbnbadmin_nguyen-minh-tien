let USER = "user";
let USER_TOKEN = "USER_TOKEN";

export const localStorageService = {
  setUserInfor: (user) => {
    let dataJson = JSON.stringify(user);
    localStorage.setItem(USER, dataJson);
  },
  getUserInfor: () => {
    let dataJson = localStorage.getItem(USER);

    if (dataJson) {
      return JSON.parse(dataJson);
    } else {
      return null;
    }
  },
  removeUserInfor: () => {
    localStorage.removeItem(USER);
  },
  setUserToken: (token) => {
    let dataJson = JSON.stringify(token);
    localStorage.setItem(USER_TOKEN, dataJson);
  },
  getUserToken: () => {
    let dataJson = localStorage.getItem(USER_TOKEN);
    if (dataJson) {
      return JSON.parse(dataJson);
    }
    return null;
  },
};
