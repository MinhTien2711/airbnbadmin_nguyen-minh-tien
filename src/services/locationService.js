import { httpService } from "./configURL";

export const locationService = {
  getLocationList: () => {
    return httpService.get(`/api/locations`);
  },
  deleteLocation: (id) => {
    return httpService.delete(`/api/locations/${id}`);
  },
  updateLocation: (id, data) => {
    return httpService.put(`/api/locations/${id}`, data);
  },
  addLocation: (data) => {
    return httpService.post("/api/locations", data);
  },
};
