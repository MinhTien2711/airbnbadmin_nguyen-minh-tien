import { httpService } from "./configURL";

export const roomService = {
  getRoomList: () => {
    return httpService.get(`/api/rooms`);
  },
  getInforRoom: (id) => {
    return httpService.get(`/api/rooms/${id}`);
  },
  deleteRoom: (id) => {
    return httpService.delete(`/api/rooms/${id}`);
  },
  updateRoom: (id, data) => {
    return httpService.put(`/api/rooms/${id}`, data);
  },
  addRoom: (data) => {
    return httpService.post("/api/rooms", data);
  },
};
