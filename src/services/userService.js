import axios from "axios";
import { BASE_URL, httpService, TOKEN_CYBERSOFT } from "./configURL";

export const userService = {
  postDangNhap: (dataLogin) => {
    return httpService.post("/api/auth/login", dataLogin);
  },
  getUserList: () => {
    return httpService.get("/api/users/pagination?skip=0&limit=0");
  },
  deleteUser: (id) => {
    return httpService.delete(`/api/users/${id}`);
  },
  getInforUser: (id) => {
    return httpService.get(`/api/users/${id}`);
  },
  updateInforUser: (id, data) => {
    return httpService.put(`/api/users/${id}`, data);
  },
  addUser: (data) => {
    return httpService.post("/api/users", data);
  },
  uploadImage: (data) => {
    return httpService.post("api/users/upload-avatar", data);
  },
};
